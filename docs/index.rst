.. Circle City River Club documentation master file, created by
   sphinx-quickstart on Tue May 19 16:11:15 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Circle City River Club's documentation!
==================================================

Rankings and score keeping app for the Circle City River Club's Texas Hold'em group.

Rules:

- Season runs May - July
- 1 Tournament per month
- Player rankings are based on points

Bonus points are attributed for the following:

- +1 Final Table
- +3 Winner
- +1 KnockOut [knock player out]
- +3 Bounty [knock out prior winner]

To start using the Project, check out the :doc:`requirements`
and next the :doc:`quick_start`.

Contents:

.. toctree::
   :maxdepth: 2



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
