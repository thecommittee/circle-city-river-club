# humanstxt.org/
# The humans responsible & technology colophon

# TEAM

    Aaron Caito -- Developer -- https://twitter.com/aaroncaito

# THANKS

    Matt McNairy for the project idea

# TECHNOLOGY COLOPHON
    Django
    HTML5 Boilerplate
    Twitter Bootstrap
    jQuery, Modernizr
