# -*- coding: utf-8 -*-
from django.contrib import admin
from . import models

@admin.register(models.Player)
class PlayerAdmin(admin.ModelAdmin):
    list_display = ("name", "email", "best_phone")
    search_fields = ["first_name", "last_name"]
