# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone
import localflavor.us.models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Player',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('first_name', models.CharField(max_length=64, verbose_name='first name')),
                ('last_name', models.CharField(max_length=64, verbose_name='last name')),
                ('played', models.PositiveIntegerField(default=0, verbose_name='games played')),
                ('created', models.DateField(default=django.utils.timezone.now, verbose_name='date created')),
                ('email', models.EmailField(null=True, max_length=255, blank=True, verbose_name='contact email')),
                ('phone', localflavor.us.models.PhoneNumberField(null=True, max_length=20, blank=True, verbose_name='contact phone number')),
                ('mobile', localflavor.us.models.PhoneNumberField(null=True, max_length=20, blank=True, verbose_name='contact mobile number')),
                ('address1', models.CharField(max_length=100, blank=True, verbose_name='address 1')),
                ('address2', models.CharField(max_length=100, blank=True, verbose_name='address 2')),
                ('address3', models.CharField(max_length=100, blank=True, verbose_name='address 3')),
                ('city', models.CharField(max_length=50, blank=True, verbose_name='city')),
                ('state', localflavor.us.models.USStateField(max_length=2, blank=True, choices=[('AL', 'Alabama'), ('AK', 'Alaska'), ('AS', 'American Samoa'), ('AZ', 'Arizona'), ('AR', 'Arkansas'), ('AA', 'Armed Forces Americas'), ('AE', 'Armed Forces Europe'), ('AP', 'Armed Forces Pacific'), ('CA', 'California'), ('CO', 'Colorado'), ('CT', 'Connecticut'), ('DE', 'Delaware'), ('DC', 'District of Columbia'), ('FL', 'Florida'), ('GA', 'Georgia'), ('GU', 'Guam'), ('HI', 'Hawaii'), ('ID', 'Idaho'), ('IL', 'Illinois'), ('IN', 'Indiana'), ('IA', 'Iowa'), ('KS', 'Kansas'), ('KY', 'Kentucky'), ('LA', 'Louisiana'), ('ME', 'Maine'), ('MD', 'Maryland'), ('MA', 'Massachusetts'), ('MI', 'Michigan'), ('MN', 'Minnesota'), ('MS', 'Mississippi'), ('MO', 'Missouri'), ('MT', 'Montana'), ('NE', 'Nebraska'), ('NV', 'Nevada'), ('NH', 'New Hampshire'), ('NJ', 'New Jersey'), ('NM', 'New Mexico'), ('NY', 'New York'), ('NC', 'North Carolina'), ('ND', 'North Dakota'), ('MP', 'Northern Mariana Islands'), ('OH', 'Ohio'), ('OK', 'Oklahoma'), ('OR', 'Oregon'), ('PA', 'Pennsylvania'), ('PR', 'Puerto Rico'), ('RI', 'Rhode Island'), ('SC', 'South Carolina'), ('SD', 'South Dakota'), ('TN', 'Tennessee'), ('TX', 'Texas'), ('UT', 'Utah'), ('VT', 'Vermont'), ('VI', 'Virgin Islands'), ('VA', 'Virginia'), ('WA', 'Washington'), ('WV', 'West Virginia'), ('WI', 'Wisconsin'), ('WY', 'Wyoming')], verbose_name='state')),
                ('zipcode', models.CharField(max_length=10, blank=True, verbose_name='zip code')),
            ],
            options={
                'verbose_name_plural': 'Players',
                'verbose_name': 'Player',
            },
        ),
    ]
