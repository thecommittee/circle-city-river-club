# -*- coding: utf-8 -*-
from django.db import models
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django.utils.timezone import now
from localflavor.us.models import PhoneNumberField, USStateField, USPostalCodeField

from . import managers


# Create your models here.
class Player(models.Model):
    # Relations
    # Attributes - Mandatory
    first_name = models.CharField(max_length=64, verbose_name=_('first name'))
    last_name  = models.CharField(max_length=64, verbose_name=_('last name'))
    played     = models.PositiveIntegerField(default=0, verbose_name=_("games played"))
    created    = models.DateField(default=now, verbose_name=_('date created'))
    # Attributes - Optional
    email      = models.EmailField(max_length=255, blank=True, null=True, verbose_name=_('contact email'))
    phone      = PhoneNumberField(blank=True, null=True, verbose_name=_('contact phone number'))
    mobile     = PhoneNumberField(blank=True, null=True, verbose_name=_('contact mobile number'))
    address1   = models.CharField(max_length=100, blank=True, verbose_name=_('address 1'))
    address2   = models.CharField(max_length=100, blank=True, verbose_name=_('address 2'))
    address3   = models.CharField(max_length=100, blank=True, verbose_name=_('address 3'))
    city       = models.CharField(max_length=50, blank=True, verbose_name=_('city'))
    state      = USStateField(blank=True, verbose_name=_('state'))
    zipcode    = models.CharField(max_length=10, blank=True, verbose_name=_('zip code'))
    # Object Manager
    objects = managers.PlayerManager()
    # Custom Properties
    @property
    def name(self):
        return self.first_name + ' ' + self.last_name

    @property
    def address(self):
        ugly = self.address1+' '+self.address2+' '+self.address3+' '+self.city+' '+self.state+' '+self.zipcode
        return " ".join(ugly.split())

    @property
    def best_phone(self):
        if self.mobile:
            return self.mobile
        else:
            return self.phone

    # Methods
    # Meta and String
    class Meta:
        verbose_name = _("Player")
        verbose_name_plural = _("Players")



# class GameStat(models.Model):
    # Relations
    # Attributes - Mandatory
    # Attributes - Optional
    # Object Manager
    # Custom Properties
    # Methods
    # Meta and String


# class Tourney(models.Model):
    # Relations
    # Attributes - Mandatory
    # Attributes - Optional
    # Object Manager
    # Custom Properties
    # Methods
    # Meta and String
